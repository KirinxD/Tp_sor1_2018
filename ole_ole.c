﻿#include <stdio.h>    // para usar printf
#include <stdlib.h>         // para usar exit y funciones de la libreria standard
#include <pthread.h>    // para usar threads



/*********************/
/*********************/
#include <semaphore.h>
sem_t a;
sem_t b;
sem_t c;
sem_t d;
sem_t e;
sem_t auxiliar;
int i=0;
int lucho=0;

/*********************/
/*********************/
pthread_mutex_t mi_mutex;




void* funcion_a ()
{	
	while (i<2){
	   	sem_wait(&a);
		printf("\tP(a)-\n");
	    	printf("\tole ole ole\n");
		sem_post(&auxiliar);
		printf("\tV(auxiliar)+\n");
	i=i+1;
	}
}

void* funcion_b ()
{	
	sem_wait(&auxiliar);
	printf("\tP(auxiliar)-\n");
		sem_wait(&b);
		printf("\tP(b)-\n");
   		printf("\tole ole ole ola\n");
	sem_post(&a);
	printf("\tV(a)+\n");
	sem_post(&c);
	printf("\tV(c)+\n");
    	pthread_exit(NULL);
}

void* funcion_c ()
{		
	
	sem_wait(&c);
	printf("\tP(c)-\n");
		sem_wait(&auxiliar);
		printf("\tP(auxiliar)-\n");
    		printf("\tcada dia te quiero mas\n");
    	sem_post(&d);
	printf("\tV(d)+\n");
    	pthread_exit(NULL);
}

void* funcion_d()
{
    	sem_wait(&d);
	printf("\tP(d)-\n");
    	printf("\tohh argentina\n");
    	sem_post(&e);
	printf("\tV(e)+\n");
    	pthread_exit(NULL);
}

void* funcion_e()
{
    	sem_wait(&e);
	printf("\tP(e)-\n");
    	printf("\tes un sentimiento no puedo parar\n");
	sem_post(&a);
	printf("\tV(a)+\n");
    	pthread_exit(NULL);
}



int main ()
{

while(lucho<10){
 printf("\t+++++++++++++++++++++++\n");
	lucho=lucho+1;
      //pthread_mutex_init ( &mi_mutex, NULL);
	sem_init(&a,0,1);
	sem_init(&b,0,1);
	sem_init(&c,0,0);
	sem_init(&d,0,0);
	sem_init(&e,0,0);
	sem_init(&auxiliar,0,0);


	pthread_t p1; //una variable de tipo pthread_t sirve para identificar al hilo que se cree
    	pthread_t p2;
	pthread_t p4;
	pthread_t p5;
	pthread_t p6;

	//int i=0;
	//while (i<4){
    //craer y lanzar ambos threads

    int rc;
	i=0;

    	rc = pthread_create(&p1,                           //identificador unico
                            NULL,                          //atributos del thread
                                funcion_a,             //funcion a ejecutar
                                NULL);                     //parametros de la funcion a ejecutar, pasado por referencia

    	rc = pthread_create(&p2,               //identificador unico
                            NULL,              //atributos del thread
                                funcion_b,        //funcion a ejecutar
                                NULL);                   //parametros de la funcion a ejecutar, pasado por referencia


	rc = pthread_create(&p4,               //identificador unico
                            NULL,              //atributos del thread
                                funcion_c,        //funcion a ejecutar
                                NULL);

	
	rc = pthread_create(&p6,               //identificador unico
                            NULL,              //atributos del thread
                                funcion_e,        //funcion a ejecutar
                                NULL);
	
	rc = pthread_create(&p5,               //identificador unico
                            NULL,              //atributos del thread
                                funcion_d,        //funcion a ejecutar
                                NULL);
	if (rc){
       		printf("Error:unable to create thread, %d \n", rc);
      		 exit(-1);
     	}

	pthread_join (p1,NULL);
	pthread_join (p2,NULL);
	pthread_join (p4,NULL);
	pthread_join (p5,NULL);
	pthread_join (p6,NULL);
}
	pthread_exit(NULL);

	sem_destroy(&a);
	sem_destroy(&b);
	sem_destroy(&c);
	sem_destroy(&d);
	sem_destroy(&e);
	sem_destroy(&auxiliar);
	}


//Para compilar:   gcc BACA.c -o ejecutable -lpthread
//Para ejecutar:   ./ejecutable
